from flask import Flask, render_template, request
# https://stackoverflow.com/questions/25466904/print-raw-http-request-in-flask-or-wsgi
app = Flask(__name__)

# https://stackoverflow.com/questions/24000729/flask-route-using-path-with-leading-slash
import werkzeug
from werkzeug.routing import PathConverter
from packaging import version

# whether or not merge_slashes is available and true
MERGES_SLASHES = version.parse(werkzeug.__version__) >= version.parse("1.0.0")

class EverythingConverter(PathConverter):
    regex = '.*?'

app.url_map.converters['everything'] = EverythingConverter

config = {"merge_slashes": False} if MERGES_SLASHES else {}
#@api.route('/records/<hostname>/<metric>/<everything:context>', **config)


@app.route("/")
def hello():
    return "UOCIS docker demo!"



@app.errorhandler(401)
def error_401(error):
    return render_template("401.html")

@app.errorhandler(403)
def error_403(error):
    return render_template("403.html")

@app.errorhandler(404)
def error_404(error):
    return render_template("404.html")


@app.route('/', defaults={'parts': ''})
@app.route('/<everything:parts>', **config)
def hey(parts):
    file_name = parts.split('/')[-1]
    if file_name[0] == '~':
        return error_403('~')
    if len(file_name) > 1 and file_name[0:2] == '..':
        return error_403('..')
    # inspired by citation #1
    if len(str(request.environ['REQUEST_URI']).split('/')) > 2:
        penultimate = str(request.environ['REQUEST_URI']).split('/')[-2]
        if penultimate == '':
            return error_403('//')
    file_ext = file_name.split('.')[-1]
    if file_ext == 'html' or file_ext == 'css':
        from pathlib2 import Path
        fp = Path('templates/' + parts)
        if fp.is_file():
            return render_template(parts)
        else:
            return error_404('dont have it') 
    else:
        return error_403('not authorized') 
            
    return error_401('above and beyond')

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
